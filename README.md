# About The Singularity Project#

"The Singularity Project" is a working title for a project that aims to explore a cross-platform evolution of The Astronomy Common Object Model (ASCOM). We are looking for developers.

The project aims to maintain full compatibility with ASCOM applications and drivers, while addressing many of its perceived weaknesses and unlocking cross-platform operation. 

The project is currently at the conceptual stage.

The project will be open source and will use the MIT License. All interested developers will be encouraged to contribute.

Governance will be based on the Benevolent Dictator model.

We will use Git to maintain a distributed source code repository. We will adopt a variant of the GitFlow branching model. All commits should be via a pull request and will be expected to undergo code review. We will provide an issue tracker that can be used for project management. We will provide a build server that will perform continuous integration builds. Unit testing and Test Driven Development will be strongly encouraged.